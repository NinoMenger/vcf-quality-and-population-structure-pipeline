#!/bin/bash

# Author: Nino Menger
# Date: 09/08/2022
# Description: This script makes an improved version of the Multisample_VCFs_Sscrofa11.1
# data set annotation. At the time or writing this data set and annotation is located at: 
# /lustre/backup/WUR/ABGC/shared/ABGC_datastore/SequenceData/Pig/Multisample_VCFs_Sscrofa11.1'


# Mandetory input:
# 1: Input file (original annotation file)
# 2: Output file (new annotation file)


# Command used fo the pipeline:
# bash Scripts/annotation.sh /lustre/backup/WUR/ABGC/shared/ABGC_datastore/SequenceData/Pig/Multisample_VCFs_Sscrofa11.1/final_species_list.tsv annotation


# Make new annotation file
touch $2


# Annotation of Asian wild boar samples. Id in first column is used to derive the country
# of origin. Note that all annotations will be removed from samples missing origin data.
# You can assume the standard annotation for these samples by removing the if statement in the
# last awk statement, and only keeping the inside of the else statement.

cat $1 | egrep 'Asian_wild' | awk  -F '\t' '{\
	if ($3 == "Japan") {print $1 "\tJapan\tNA"} \
	else if ($3 == "Thailand") {print $1 "\tThailand"} \
	else if ($3 == "China" && $1 ~ /WB29/) {print $1 "\tChina, south"} \
	else if ($3 == "China" && $1 ~ /WB30/) {print $1 "\tChina, north"} \
	else {print $1 "\tNA"}}' | \
	awk -F '\t' '{if ($2 == "NA") {print $1 "\tNA\tNA\tNA\tNA"} \
	else {print $1 "\tSus scrofa\tWild\tAsia\t" $2}}' >> $2


# Annotation of European wildboar. Id in first column is used to derive the country
# of origin. Note that all annotations will be removed from samples missing origin data.
# You can assume the standard annotation for these samples by removing the if statement in the
# last awk statement, and only keeping the inside of the else statement.

cat $1 | egrep 'European_wild' | awk -F '\t' '{
	if ($1 ~ /WB21/) {print $1 "\tNetherlands, Veluwe"} \
	else if ($1 ~ /WB22/) {print $1 "\tNetherlands, Mijnweg"} \
	else if ($1 ~ /WB25/) {print $1 "\tFrance"} \
	else if ($1 ~ /WB26/) {print $1 "\tSwitzerland"} \
	else if ($1 ~ /WB28/) {print $1 "\tItaly, Cilento"} \
	else if ($1 ~ /WB31/) {print $1 "\tGreece, Central"} \
	else if ($1 ~ /WB32/) {print $1 "\tGreece, North"} \
	else if ($1 ~ /WB33/) {print $1 "\tGreece, Samos"} \
	else if ($1 ~ /WB42/) {print $1 "\tAustria"} \
	else if ($1 ~ /WB44/) {print $1 "\tPoland, north-east"} \
	else if ($1 ~ /WB46/) {print $1 "\tHungary"} \
	else {print $1 "\tNA"}}' | \
	awk -F '\t' '{ if ($2 == "NA") {print $1 "\tNA\tNA\tNA\tNA"} \
	else {print $1 "\tSus scrofa\tWild\tEurope\t" $2}}' >> $2


# Annotation of Asian domesticated samples. Annotation in origninal is satisfactory.
# The information only gets reorderd.
cat $1 | egrep 'Asian_dom' | awk -F '\t' '{ \
	if ($3 == "Leping_spotted") {print $1 "\tSus scrofa\tLeping spotted\tAsia\tNA"} \
	else {print $1 "\tSus scrofa\t" $3 "\tAsia\tNA"} \
	}' >> $2


# Annotation of European domesticated samples. Annotation in orginal is quite
# satisfactory. Leicoma is an Germand breed, and thus is considerd German. UK
# has been written in full.
cat $1 | egrep 'European_local' | awk -F '\t' '{
	if ($4 == "Leicoma") {print $1 "\t" $4 "\tGermany"} \
	else if ($4 == "UK") {print $1 "\t" $3 "\tUnited Kingdom"} \
	else {print $1 "\t" $3 "\t" $4}}' | \
	awk -F '\t' '{print $1 "\tSus scrofa\t" $2 "\tEurope\t" $3}' >> $2


# Annotation of western comercial breeds. Annotation in origninal is satisfactory.
# The information only gets reorderd. One exeption is made for the samples labeled
# as: 'Large_White'. These samples get relabeld as 'Large White'.
# Note that all annotations will be removed from samples missing origin data.
# You can assume the standard annotation for these samples by removing the if statement in the
# last awk statement, and only keeping the inside of the else statement.
cat $1 | egrep 'Western_com' | awk -F '\t' '{
	if ($3 == "Large_White") {print $1 "\tLarge White"}
	else {print $1 "\t" $3}}' | \
	awk -F '\t' '{ if ($2 == "NA") {print $1 "\tNA\tNA\tNA\tNA"} \
	else {print $1 "\tSus scrofa\t" $2 "\tCommercial\tNA"}}' >> $2



# Annotation of all other samples. Information in third column is used to sort Pygmyhog,
# ISEA Sus and African species. Notably, exeptions have been made for some Sus barbatus, 
# S. cebifrons and S. celebensis samples. These samples have been excluded based on 
# certain pettern in their id (firs column). These samples where not representive of
# the specie which they were orginally assigned.
# Babyrussa was selected based on the 4th column.
cat $1 | awk -F '\t' '{if ($2 == "NA"){print $0}}' | awk -F '\t' '{
	if ($4 == "Barbyrussa") {print $1 "\tBabyrussa\tWild\tISEA\tNA"}
        else if ($3 == "Pygmyhog") {print $1 "\tPorcula salvania\tWild\tAsia\tNA"}
        else if ($3 == "Pecari tajacu") {print $1 "\tPecari tajacu\tWild\tAmerica\tNA"}
        else if ($3 == "Phacochoerus africanus") {print $1 "\tPhacochoerus africanus\tWild\tAfrica\tNA"}
        else if ($3 ~ /porcus/) {print $1 "\tPotomochoerus porcus\tWild\tAfrica\tNA"}
        else if ($3 == "Potomochoerus larvatus") {print $1 "\tPotomochoerus larvatus\tWild\tAfrica\tNA"}
        else if ($3 == "sus_scrofa") {print $1 "\tSus scrofa\tWild\tISEA\tNA"}
        else if ($3 == "Sus barbatus" && $1 ~ /SBSB/ && $1 !~ /U01/) {print $1 "\tSus barbatus\tWild\tISEA\tNA"}
        else if ($3 == "Sus cebifrons" && $1 ~ /SCEB/) {print $1 "\tSus cebifrons\tWild\tISEA\tNA"}
        else if ($3 == "Sus celebensis" && $1 ~ /SCEL/) {print $1 "\tSus celebensis\tWild\tISEA\tNA"}
        else if ($3 == "Sus verrucosus") {print $1 "\tSus verrucosus\tWild\tISEA\tNA"}
        else {print $1 "\tNA\tNA\tNA\tNA"}}' >> $2
