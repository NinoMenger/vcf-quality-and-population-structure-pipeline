#!/bin/bash

# Author: Nino Menger
# Date: 15/12/2022
# Description: This script estimates the sex of an individual based on a idpepth analysis
# The script calculates the mean sequencing depth of all non X chromosomes.
# If the sequencing depth of the X chromosome is lower than 0.75 times the mean it
# is concidderd a male.

# Mandetory input:
# 1: Input file (idepth file)


anno=$(cat $1 | \
awk '{if ($3=="X"){arrX[$1]=$2} else{ arr[$1]+=$2}} \
	END{for (key in arr){if ((arr[key]/18 * 0.75 >= arrX[key]) == "0")\
		{sex="Female"}else{sex="Male"} print key, sex}}'
)

echo $anno
