"""
Created by: Nino Menger
Date: 10/08/2022
Description:
"""

# Reffrence to the config file. The user can change some settings within this file.
configfile: "SnakefileConfig.yaml"


"""
The SD_CHROMOSOMES config variable stores the amount of chromosomes that the user wants to use in
the analysis. The code below creates a list of chromosome numbers with the length of the
SD_CHROMOSOMES config variable. The code keeps into account that there are only 19 chromosomes, including
a X chromosome
"""

SD_CHROMOSOMES = config["SD_CHROMOSOMES"]


rule all:
	input:
		[config["PCAVIS"] + "variance_" + i + ".png" for i in config["GROUPS"].keys()],
		expand(config["HEATMAP"], GROUP=config["GROUPS"].keys())


rule seqDepth:
	input:
		vcfs = [config["INFILES"].format(CHR=i) for i in SD_CHROMOSOMES]
	output:
		#temp("out.log"),
		idepth = config["SEQDEPTH"]
	params:
		SD_CHROMOSOMES
	shell:
		"""
		# Load all modules
		module load vcftools
		
		touch {output.idepth}

		chromosomes=$(echo {params} | tr -d '[],')
		
		# run vcftools depth algoritm for all chromosomes and save output to the output file
		
		index=1
		for file in {input.vcfs}
		do
			chr=$(echo $chromosomes | awk -v var=$index '{{print($var)}}')

			echo Deriving mean sequencing depth from: ${{file}}
			vcftools --gzvcf ${{file}} --depth --stdout | awk '(NR > 1)' | \
				awk -v Chr=$chr '{{print $1, $3, Chr}}' >> {output.idepth}
			
			index=$(($index+1))
		done
		
		# Unload all modules
		module unload vcftools
		"""

def get_title(wildcards):
        return config["GROUPS"].get("{wc}".format(wc=wildcards)).get("Title")


rule heatmap:
	input:
		idepth = {rules.seqDepth.output.idepth},
		anno = config["ANNOTATIONFILE"]
	output: 
		png = config["HEATMAP"].format(GROUP="{group}"),
		keepfile = temp("keep_hm_{group}.temp")
	params:
		dict = config["GROUPS"],
		group = "{group}",
		title = get_title
	shell:
		"""
		# get right values from dictonary and return as tab seperated list
		regex="$(python -c "print('\t'.join([{params.dict}.get('{params.group}').get(i) for i in {params.dict}.get('{params.group}').keys() if not i in ['Title', 'POI']]))")"              
		# Return samples of which trait match with a group/population
                cat {input.anno} | awk -F "\t" -v regex="$regex" 'BEGIN{{split(regex, regexArr, "\t")}} {{if ( \
                $2 ~ regexArr[1] && \
                $3 ~ regexArr[2] && \
                $4 ~ regexArr[3] && \
                $5 ~ regexArr[4] \
                ) {{print $1}}}}' > {output.keepfile}

		echo {output.keepfile}
		cat {output.keepfile}
		# Load modules
                module unload gcc
                module load R/4.2.0
                Rscript Scripts/heatmap.R \
                        --annotation {input.anno} --idepth {input.idepth} --keepfile {output.keepfile} \
			--column_names "ID\tSpecie\tBreed\tContinent\tCountry" \
			--output "{output.png}" --title "{params.title}" 


                # Unload modules
                module unload R
		"""

rule PCA:
	input: 
		vcf=config["INFILES"].format(CHR=config["PCA_CHROMOSOME"]),
		anno=config["ANNOTATIONFILE"]
	output: expand("{base}{{group}}.{ext}", base=config["PCA"], ext=["eigenvec", "eigenval"]),
		temp(expand("{base}{{group}}.{ext}", base=config["PCA"], ext= ["prune.in", "prune.out", "nosex", "log"])),
		keepfile = temp("keep_{group}.temp")
	params: 
		dict = config["GROUPS"], 
		group = "{group}",
		base = config["PCA"] + "{group}"
	shell: 
		"""
		# Load modules
		module load plink
		
		# get right values from dictonary and return as tab seperated list
		# This line should work but has to be tested in the future (it had to be changed do to new data structure)
		regex="$(python -c "print('\t'.join([{params.dict}.get('{params.group}').get(i) for i in {params.dict}.get('{params.group}').keys() if not i in ['Title', 'POI']]))")"
		# Return samples of which trait match with a group/population
		cat {input.anno} | awk -F "\t" -v regex="$regex" 'BEGIN{{split(regex, regexArr, "\t")}} {{if ( \
		$2 ~ regexArr[1] && \
		$3 ~ regexArr[2] && \
		$4 ~ regexArr[3] && \
		$5 ~ regexArr[4] \
		) {{print $1, $1}}}}' > {output.keepfile}
	
		echo {output.keepfile}
		cat {output.keepfile}

		# Linkage pruning see for explenation: https://speciationgenomics.github.io/pca/
		plink --vcf {input.vcf} --double-id --allow-extra-chr \
        	--set-missing-var-ids @:# --keep {output.keepfile} \
        	--indep-pairwise 50 5 0.1 --out {params.base}
		
		# PCA plot analysis
		plink --vcf {input.vcf} --double-id --allow-extra-chr --set-missing-var-ids @:# \
		--extract {params.base}.prune.in \
		--pca --out {params.base} \
		--keep {output.keepfile}
		
		# Unload modules
		module unload plink
		"""

def funct(wildcards):
	return config["GROUPS"].get("{wc}".format(wc=wildcards))


def get_POIs(wildcards):
	return config["GROUPS"].get("{wc}".format(wc=wildcards)).get("POI")


def get_title(wildcards):
        return config["GROUPS"].get("{wc}".format(wc=wildcards)).get("Title")


def get_header(wildcards):
	return '\t'.join(
		[i for i in config["GROUPS"].get("{wc}".format(wc=wildcards)).keys()
		if not i in ['Title', 'POI']]
	)


rule PCA_visualisation:
	input: 
		eigenvec = config["PCA"] + "{group}" + ".eigenvec",
		eigenval = config["PCA"] + "{group}" + ".eigenval",
		anno = config["ANNOTATIONFILE"]
	output:
		variance = config["PCAVIS"] + "variance_{group}.png"
	params:
		POIs = get_POIs,
		header = get_header,
		group = "{group}",
		title = get_title,
		pca = config["PCAVIS"]
	shell:
		"""
		# Load modules
		module unload gcc
		module load R/4.2.0
		Rscript Scripts/PCAVisualisation.R --eigenvec {input.eigenvec} --eigenval {input.eigenval} \
			--categories "{params.POIs}" --pca {params.pca} --variance {output.variance} --group {params.group} \
			--annotation {input.anno} --column_names "ID\t{params.header}" --title "{params.title}" 

	
		# Unload modules
		module unload R 
		"""
