#!bin/bash

snakemake --cluster "sbatch --job-name {cluster.name} --comment {cluster.comment} \
        --mail-user {cluster.adress} --mail-type {cluster.type} \
        --output {cluster.output} --error {cluster.error} \
        --qos {cluster.priority} --time {cluster.time} --ntasks {cluster.tasks} \
        --cpus-per-task {cluster.cpus} --mem-per-cpu {cluster.memory}" \
        --cluster-config clusterConfig.yaml --jobs 10
